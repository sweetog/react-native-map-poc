/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import MapScreen from './src/screens/mapscreen';

const App = () => {
  return (
    <>
      <MapScreen />
    </>
  );
};

export default App;
