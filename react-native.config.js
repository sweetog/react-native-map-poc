// react-native.config.js
module.exports = {
  dependencies: {
    'react-native-vector-icons': {
      platforms: {
        android: null, // disable Android platform, other platforms will still autolink
        ios: null,
      },
    },
    'react-native-maps': {
      platforms: {
        android: null, // disable Android platform, other platforms will still autolink
        ios: null,
      },
    },
  },
};
