# Install Package
   npm install
   or
   yarn install
# Run
   - android
     react-native run-android
     or Android Studio
   - ios
	 cd ios
	 pod install
	 cd..
	 react-native run-ios
	 or Xcode
