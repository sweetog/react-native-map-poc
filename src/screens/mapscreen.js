import React, { useState } from 'react';
import {
  View,
  StatusBar,
  StyleSheet,
  TextInput,
  Dimensions,
  TouchableOpacity,
  FlatList,
  Text,
  Alert,
} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import Ionicons from 'react-native-vector-icons/Ionicons';

const OpenLocationCode = require('open-location-code').OpenLocationCode;
const openLocationCode = new OpenLocationCode();
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
const defaultLat = 37.78825;
const defaultLng = -122.4324;
const defaultLatDelta = 0.0922;
const defaultLngDelta = 0.0421;
const MapScreen = () => {
  const [code, setCode] = useState('');
  const [region, setRegion] = useState({
    latitude: defaultLat,
    longitude: defaultLng,
    latitudeDelta: defaultLatDelta,
    longitudeDelta: defaultLngDelta,
  });
  const [histories, setHistories] = useState([]);
  const [selected, setSelected] = useState(false);
  const searchPress = value => {
    let coord;
    setSelected(true);
    try {
      coord = openLocationCode.decode(value);
    } catch (e) {
      Alert.alert('INVALID CODE!');
      return;
    }
    if (coord === null || coord === undefined) {
      return;
    }
    setRegion({
      latitude: coord.latitudeCenter,
      longitude: coord.longitudeCenter,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
    });
    let arr = histories;
    arr.unshift({
      code: value,
      lat: coord.latitudeCenter,
      lng: coord.longitudeCenter,
    });
    setHistories(arr);
  };
  const handleClickHistory = item => {
    setSelected(true);
    setCode(item.code);
    searchPress(item.code);
  };
  const setCodeFilter = value => {
    if (value === null || value === undefined) {
      return;
    }
    if (value.length > 14) {
      return;
    }
    value = value.replace(/[^A-Za-z0-9+]/g, '');
    setSelected(false);
    setCode(value);
  };
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <View style={styles.container}>
        <MapView
          style={styles.map}
          region={region}
          onRegionChangeComplete={setRegion}>
          <Marker
            coordinate={{
              latitude: region.latitude,
              longitude: region.longitude,
            }}
          />
        </MapView>
      </View>
      <View style={styles.searchSection}>
        <TextInput
          style={styles.searchInput}
          placeholder={'Enter a plus code'}
          underlineColorAndroid="transparent"
          value={code}
          onChangeText={text => setCodeFilter(text)}
          onFocus={() => setSelected(true)}
        />
        <TouchableOpacity onPress={() => searchPress(code)}>
          <Ionicons
            style={styles.searchIcon}
            name="ios-search"
            size={20}
            color="#000"
          />
        </TouchableOpacity>
      </View>
      {histories.length && !selected ? (
        <FlatList
          style={styles.historiesContainer}
          data={histories}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item}) => (
            <TouchableOpacity
              style={styles.history}
              onPress={() => handleClickHistory(item)}>
              <Text style={styles.historyText}>{item.code}</Text>
              <View style={styles.line} />
            </TouchableOpacity>
          )}
        />
      ) : null}
      <Text style={styles.coordText}>
        Latitude: {Number(region.latitude).toFixed(2)} Longitude{' '}
        {Number(region.longitude).toFixed(2)}
      </Text>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  map: {
    width: deviceWidth,
    height: deviceHeight,
  },
  searchInput: {
    flex: 1,
    padding: 10,
    backgroundColor: '#fff',
  },
  searchSection: {
    alignSelf: 'center',
    position: 'absolute',
    width: '90%',
    marginTop: 40,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  searchIcon: {
    padding: 10,
  },
  historiesContainer: {
    backgroundColor: '#fff',
    alignSelf: 'center',
    width: '90%',
    marginLeft: 0,
    marginRight: 0,
    maxHeight: deviceHeight * 0.3,
    top: 90,
    position: 'absolute',
  },
  history: {
    fontSize: 16,
    paddingVertical: 5,
  },
  historyText: {
    fontSize: 15,
    marginLeft: 10,
    marginRight: 10,
    color: '#000',
  },
  line: {
    height: 0.5,
    marginLeft: 10,
    marginRight: 10,
    backgroundColor: '#000',
    marginTop: 5,
  },
  coordText: {
    position: 'absolute',
    width: '90%',
    bottom: 15,
    alignSelf: 'center',
    backgroundColor: '#fff',
    textAlign: 'center',
  },
});

export default MapScreen;
